package com.avsautomation.hefboilerms;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    LineChart gasBeforeBoilerChart;
    LineChart drumWaterLevelChart;
    LineChart supheatSteamPressureAfterBoilerChart;
    LineChart tempSteamAfterBoilerChart;
    LineChart temp2SteamAfterBoilerChart;
    LineChart flowSupheatSteamAfterBoilerChart;
    LineChart flowFeedWaterOnBoilerChart;
    LineChart tempFeedwaterAndExhGasesChart;
    LineChart pressFeedwaterOnBoilerChart;
    LineChart flowrateAirBeforeBurnersChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupConnectionFactory();

        gasBeforeBoilerChart = (LineChart)findViewById(R.id.gas_before_boiler_chart);
        drumWaterLevelChart = (LineChart)findViewById(R.id.drum_water_level_chart);
        supheatSteamPressureAfterBoilerChart = (LineChart)findViewById(R.id.supheat_steam_pressure_after_boiler_chart);
        tempSteamAfterBoilerChart = (LineChart)findViewById(R.id.temp_steam_after_boiler_chart);
        temp2SteamAfterBoilerChart = (LineChart)findViewById(R.id.temp2_steam_after_boiler_chart);
        flowSupheatSteamAfterBoilerChart = (LineChart)findViewById(R.id.flow_supheat_steam_after_boiler_chart);
        flowFeedWaterOnBoilerChart = (LineChart)findViewById(R.id.flow_feedwater_on_boiler_chart);
        tempFeedwaterAndExhGasesChart = (LineChart)findViewById(R.id.temp_feedwater_and_exh_gases_chart);
        pressFeedwaterOnBoilerChart = (LineChart)findViewById(R.id.press_feedwater_on_boiler_chart);
        flowrateAirBeforeBurnersChart = (LineChart)findViewById(R.id.flowrate_air_before_burners_chart);

        gasBeforeBoilerChart.setData(new LineData());
        drumWaterLevelChart.setData(new LineData());
        supheatSteamPressureAfterBoilerChart.setData(new LineData());
        tempSteamAfterBoilerChart.setData(new LineData());
        temp2SteamAfterBoilerChart.setData(new LineData());
        flowSupheatSteamAfterBoilerChart.setData(new LineData());
        flowFeedWaterOnBoilerChart.setData(new LineData());
        tempFeedwaterAndExhGasesChart.setData(new LineData());
        pressFeedwaterOnBoilerChart.setData(new LineData());
        flowrateAirBeforeBurnersChart.setData(new LineData());

        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                String message = msg.getData().getString("msg");
                TextView tv = (TextView)findViewById(R.id.caption);

                SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss");

                tv.setText( sdf.format(Calendar.getInstance().getTime()) + " Получены значения:" );

                String[] values = message.split(" ");

                char firstDigit = message.charAt(0);
                char secondDigit = message.charAt(1);

                float value = Float.parseFloat( values[2] );

                if(secondDigit == '0')//10
                {
                    TextView tv1 = (TextView)findViewById(R.id.flowrate_air_before_burners);
                    tv1.setText(values[2]);
                    addDynamicValue( value, flowrateAirBeforeBurnersChart, "Расход воздуха до горелок" );

                    if( chartEnd - chartStart > 100 ) {
                        chartStart++;
                    }
                }
                else
                {
                    TextView tv2 = null;

                    switch (firstDigit){
                        case '1': {
                            chartEnd++;

                            tv2 = (TextView)findViewById(R.id.gas_before_boiler_view);
                            addDynamicValue( value, gasBeforeBoilerChart, "Расход газа на котле" );

                            break;
                        }
                        case '2': {
                            tv2 = (TextView)findViewById(R.id.drum_water_level_view);
                            addDynamicValue( value, drumWaterLevelChart, "Уровень воды в барабане" );
                            break;
                        }
                        case '3': {
                            tv2 = (TextView)findViewById(R.id.supheat_steam_pressure_after_boiler);
                            addDynamicValue( value, supheatSteamPressureAfterBoilerChart, "Давление перегретого пара после котла" );
                            break;
                        }
                        case '4': {
                            tv2 = (TextView)findViewById(R.id.temp_steam_after_boiler_view);
                            addDynamicValue( value, tempSteamAfterBoilerChart, "Температура пара после котла" );
                            break;
                        }
                        case '5': {
                            tv2 = (TextView)findViewById(R.id.temp2_steam_after_boiler_view);
                            addDynamicValue( value, temp2SteamAfterBoilerChart, "Температура 2 пара после котла" );
                            break;
                        }
                        case '6': {
                            tv2 = (TextView)findViewById(R.id.flow_supheat_steam_after_boiler_view);
                            addDynamicValue( value, flowSupheatSteamAfterBoilerChart, "Расход перегретого пара после котла" );
                            break;
                        }
                        case '7': {
                            tv2 = (TextView)findViewById(R.id.flow_feedwater_on_boiler_view);
                            addDynamicValue( value, flowFeedWaterOnBoilerChart, "Расход питательной воды в котле" );
                            break;
                        }
                        case '8': {
                            tv2 = (TextView)findViewById(R.id.temp_feedwater_and_exh_gases_view);
                            addDynamicValue( value, tempFeedwaterAndExhGasesChart, "Температура питательной воды и исходящих газов" );
                            break;
                        }
                        case '9': {
                            tv2 = (TextView)findViewById(R.id.press_feedwater_on_boiler_view);
                            addDynamicValue( value, pressFeedwaterOnBoilerChart, "Давление питательной воды на котле" );
                            break;
                        }
                    }
                    if( tv2 != null )
                        tv2.setText(values[2]);
                }
            }
        };

        subscribe(handler);

    }

    static final SimpleDateFormat IN_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss", Locale.US);
    static final SimpleDateFormat OUT_DATE_FORMAT = new SimpleDateFormat("hh:mma", Locale.US);

    /**
     * To format the time.
     *
     * @param strDate The input date in HH:mm:ss format.
     * @return The output date in hh:mma format.
     */
    private String formatTime(String strDate) {
        try {
            Date date = IN_DATE_FORMAT.parse(strDate);
            return OUT_DATE_FORMAT.format(date);

        } catch (Exception e) {
            return "N/A";
        }
    }

    private int chartStart = 0;
    private int chartEnd = 1;

    private void addDynamicValue(float value, LineChart chart, String caption) {
        LineData data = chart.getData();
        if( data != null )
        {
            ILineDataSet set = data.getDataSetByIndex(0);

            if( set == null )
            {
                set = createSet(caption);
                data.addDataSet(set);
            }

            if( set.getEntryCount() > 100 )
                removeFirstEntry(chart);

            data.addXValue(chartEnd + "");
            data.addEntry( new Entry(value, chartEnd), 0);

            chart.notifyDataSetChanged();
            chart.setVisibleXRangeMinimum(50);

            chart.setVisibleXRangeMaximum(100);
            chart.moveViewTo(data.getXValCount(), 0f, YAxis.AxisDependency.LEFT);
        }
    }

    private void removeFirstEntry(LineChart chart) {

        LineData data = chart.getData();

        if(data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);

            if (set != null) {
                Entry e = set.getEntryForXIndex(0);
                data.removeEntry(e, 0);
            }
        }
    }

    private ILineDataSet createSet(String caption) {

        LineDataSet set = new LineDataSet(null, caption);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.rgb(0,255,0));
        set.setDrawCircles(false);
        set.setDrawValues(false);
        return set;
    }

    ConnectionFactory factory = new ConnectionFactory();
    private void setupConnectionFactory(){
        String uri = "amqp://ms:msqA115@185.48.149.157:5672";
        try{
            factory.setAutomaticRecoveryEnabled(false);
            factory.setUri(uri);
        }
        catch(KeyManagementException | NoSuchAlgorithmException | URISyntaxException e1)
        {
            e1.printStackTrace();
        }

    }

    private Thread subscribeThread;

    private
        void subscribe(final Handler handler)
        {
            subscribeThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Connection connection = factory.newConnection();
                            Channel channel = connection.createChannel();
                            channel.basicQos(1);
                            channel.exchangeDeclare("vals","fanout");
                            AMQP.Queue.DeclareOk q = channel.queueDeclare();
                            channel.queueBind(q.getQueue(), "vals", "");
                            QueueingConsumer consumer = new QueueingConsumer(channel);
                            channel.basicConsume(q.getQueue(), true, consumer);

                            while (true) {
                                QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                                String message = new String(delivery.getBody());
                                Message msg = handler.obtainMessage();
                                Bundle bundle = new Bundle();
                                bundle.putString("msg", message);
                                msg.setData(bundle);
                                handler.sendMessage(msg);
                            }
                        } catch (InterruptedException e) {
                            break;
                        } catch (Exception e1) {
                            Log.d("", "Connection broken: " + e1.getClass().getName());
                            try {
                                Thread.sleep(5000); //sleep and then try again
                            } catch (InterruptedException e) {
                                break;
                            }
                        }
                    }
                }
            });
            subscribeThread.start();
        }

    @Override
    protected  void onDestroy(){
        super.onDestroy();
        subscribeThread.interrupt();
    }
}
